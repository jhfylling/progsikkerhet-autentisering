const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const cors = require('cors');
const fs = require('fs');

const app = express();

// app.use(bodyParser.json());

const port = 8081;

var con = mysql.createConnection({
    host: "jhfdev.net",
    user: "dev",
    password: "a44ZQpai8OvAb0z9",
    database: "dev"
});

const privateKey = fs.readFileSync('./keys/jwt.key');
const publicKey = fs.readFileSync('./keys/jwt.key.pub');

con.connect((err) => {
    if (err) {
        console.log('Could not connect to database');
    } else {
        console.log('Connected to MySQL server');
    }
});

// app.options('/login', function (req, res) {
    //     res.setHeader("Access-Control-Allow-Origin", "*");
    //     res.setHeader('Access-Control-Allow-Methods', '*');
    //     res.setHeader("Access-Control-Allow-Headers", "*");
    //     res.end();
    // });

    // app.get('/', (req, res) => res.send('hei'));
app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": true,
    "optionsSuccessStatus": 204
  }));

app.post('/user', (req, res) => {
    con.connect(function (err) {
        if (err) throw err;
        console.log("Connected to mysql!");
        let { user } = req.body;

        const salt = toBase64(crypto.randomBytes(128));

        pbkdf2(toBase64(user.password), salt)
            .then(() => {
                con.query('INSERT INTO user (email, password, salt) VALUES (?,?,?)',
                    [user.email, key.toString('base64'), salt],
                    (err, queryResult) => {
                        if (!err) {
                            console.log('affected rows:', queryResult.affectedRows);
                            if (queryResult.affectedRows === 1) {
                                res.send('New user registered');
                            } else {
                                res.status(500);
                                res.send('Contact admin');
                            }
                        } else {
                            res.status(500);
                            res.send('Contact admin');
                        }
                    });
            })
            .catch((err) => {
                console.log(err);
                res.status(500);
                res.send('Something went wrong');
            });
    });
});

app.get('/login', (req, res) => {
    const { user } = req.body;
    getSalt(user.email)
        .then((salt) => {
            pbkdf2(toBase64(user.password), salt)
                .then((password) => {
                    getUser(user.email, password)
                        .then((result) => {
                            if (result) {
                                console.log('result: ', result.id, result.email);
                                let user = {id: result.id, email: result.email};
                                let token = jwt.sign({user: user}, privateKey, {algorithm: 'RS256', expiresIn: '2d', issuer: 'johan'});
                                res.status(200);
                                res.json({token: token});
                            }
                        })
                        .catch((err) => {
                            console.log('Error getting user:', err);
                            res.status(200);
                            res.send('Could not log in');
                        });
                })
                .catch((err) => {
                    console.log('Error creating pbkdf2: ', err);
                    res.status(500);
                    res.send('Something went wrong. Our dev gods are on it.');
                });
        })
        .catch((err) => {
            console.log('Error getting salt: ', err);
            res.status(200);
            res.send('Invalid email or internal server error');
        })
});

app.use('/api', (req, res, next) => {
    const token = req.headers['x-access-token'];
    try{
        let verified = jwt.verify(token, publicKey, {algorithms: ["RS256"], expiresIn: '2d', issuer: 'johan'});
        console.log('verified: ', verified);
        // res.status(200);
        // res.json({"message": "authorized"});
        next();
    } catch (err) {
        console.log('Error verifying: ', err.message);
        res.status(401);
        res.json({"message": "not logged in"});
    };
});


app.get('/api/joke', (req, res) => {
    let jokes = [
        "Today at the bank, an old lady asked me to help check her balance. So I pushed her over.",
        "I bought some shoes from a drug dealer. I don't know what he laced them with, but I've been tripping all day.",
        "I told my girlfriend she drew her eyebrows too high. She seemed surprised.",
        "I'm so good at sleeping. I can do it with my eyes closed.",
        "My boss told me to have a good day.. so I went home.",
        "The other day, my wife asked me to pass her lipstick but I accidentally passed her a glue stick. She still isn't talking to me.",
        "What did the pirate say when he turned 80 years old? Aye matey.",
        "I know a lot of jokes about unemployed people but none of them work.",
        "My wife accused me of being immature. I told her to get out of my fort.",
        "When a deaf person sees someone yawn do they think it’s a scream?"
    ];

    let i = Math.floor(Math.random() * 10);

    console.log("joke:", jokes[i]);

    res.status(200);
    res.json({"message": jokes[i]});
});

let toBase64 = (bytes) => {
    return (new Buffer(bytes)).toString('base64');
}

let pbkdf2 = (pw, salt) => {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(pw, salt, 2048, 20, 'sha1', (err, key) => {
            if (!err) {
                resolve(key.toString('base64'))
            } else {
                reject('Error creating pbkdf2 hash' + err);
            }
        });
    });
}

let getSalt = (email) => {
    return new Promise((resolve, reject) => {
        con.query('SELECT salt FROM user WHERE email=?', [email], (err, result) => {
            if (!err) {
                if (result.length > 0) {
                    resolve(result[0].salt);
                } else {
                    reject('No user with email: ' + email);
                }
            } else {
                reject('MySQL Query error: ' + err);
            }
        });
    });
}

let getUser = (email, password) => {
    return new Promise((resolve, reject) => {
        con.query('SELECT id, email FROM user WHERE email=? AND password=?', [email, password], (err, result) => {
            if (!err) {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject('No user with email & password combination matches');
                }
            } else {
                reject('MySQL Query error: ' + err);
            }
        });
    });
}

app.listen(port, () => console.log('Rest service running on port', port));